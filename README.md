![a](/uploads/3587f09bf1f2909780bb6691f3407b1e/a1dca21d539f923d9021230dbefa8859d8bc6bcc.png)


**Unison**  bash converter script.

*Dependencies:*
1. youtube-dl 
2. ffmpeg 


*To use it make it executable and pass a file with a youtube link playlist, cannel per line.*

> Example`  name playlist.link
> * https://www.youtube.com/user/MarvellousDub/videos 
> * https://www.youtube.com/user/MrSuicideSheep 
> * https://www.youtube.com/watch?v=SmAEwy0f_1Y  



*to use script write in the terminal*
> * `chmod +x unison`

> * `bash unison ogg` or `./unison mp3`  for get ogg
> * `bash unison m4a`  or `./unison m4a`   for get m4a

add a playlist to download in the format **.link**  !


note*  when you convert to flac you will not get quality FLAC , I advise you not to use the command
> * `bash unison flac` or `./unison flac`  for get flac

